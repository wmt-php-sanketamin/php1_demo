<?php
    $x = 0;
    // While loop
    while($x < 100) {
        echo "in While Loop : $x <br>";
        $x+=10;
    }
    // Do...While Loop
    $y=0;
    do {
        echo "in do while loop: $y <br>";
        if($y == 2){
            break;
        }
        $y++;
    }while ($y <= 5);

    // For loop
    for ($x = 0; $x <= 10; $x++) {
        if($x == 4){
            continue;
        }   
        echo "in for loop: $x <br>";
    }
    // Foreach loop
    $age = array("sanket"=>"20", "bansal"=>"28", "savan"=>"18");
    foreach($age as $x => $x_value) {
        echo "in for each loop <br>";
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }
?>