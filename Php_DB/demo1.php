<?php
    $mysql_host = "localhost";
    $mysql_user = "root";
    $mysql_password = "";

    // Connection of database and select the database
    $con = mysqli_connect($mysql_host,$mysql_user,$mysql_password);
    mysqli_select_db($con, "student");

    // insert into the database
    // $sql = "INSERT INTO user_info (fname, surname, username, pwd)
    // VALUES ('John', 'Doe', 'john@exa', '123456')";

    // if ($con->query($sql) === TRUE) {
    //     $last_id = $con->insert_id;
    // echo "New record created successfully.  Last inserted ID is: " . $last_id;
    // } else {
    // echo "Error: " . $sql . "<br>" . $con->error;
    // }


    //Prepare and Bind
    $stmt = $con->prepare("INSERT INTO user_info (fname, surname, username, pwd) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("ssss", $firstname, $surname, $username , $pwd);

    // set parameters and execute
    $firstname = "The";
    $surname = "Rock";
    $username = "rock@exa";
    $pwd = "123456";
    $stmt->execute();

    $firstname = "Mary";
    $surname = "Moe";
    $username = "mary@exa";
    $pwd = "123456";
    $stmt->execute();

    echo "New Records Created succesfully.";
?>
