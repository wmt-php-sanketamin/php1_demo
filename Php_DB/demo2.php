<?php
    $mysql_host = "localhost";
    $mysql_user = "root";
    $mysql_password = "";

    // Connection of database and select the database
    $con = mysqli_connect($mysql_host,$mysql_user,$mysql_password);
    mysqli_select_db($con, "student");

    if (!$con) {
        die("Connection failed: " . mysqli_connect_error());
    }
    // $sql = "SELECT * FROM user_info ORDER BY fname"; this is the example of Order by

    //Select Data
    // $sql = "SELECT * FROM user_info WHERE surname='Doe'";
    // $res = mysqli_query($con, $sql);

    // if (mysqli_num_rows($res) > 0) {
    //     // output data of each row
    //     while($row = mysqli_fetch_assoc($res)) {
    //         echo "id: " . $row["id"]. " - Name: " . $row["fname"]. " " . $row["surname"]. "  Username:- " . $row["username"] . "<br>";
    //     }
    // }
    // else {
    //     echo "0 results";
    // }

    // delete a record
    // DELETE FROM user_info WHERE id=3

    //Update a Record
    $sql = "UPDATE user_info SET surname='Amin' WHERE id=4";

    if ($con->query($sql) === TRUE) {
    echo "Record Updated successfully";
    } else {
    echo "Error Updating record: " . $con->error;
    }

?>
