<?php
    //Simple function with calling func name.
    function writeMsg() {
    echo "Hello world!";
    }

    writeMsg(); // call the function

    // Function with parameters.
    function addFunction($num1, $num2) {
        $sum = $num1 + $num2;
        echo "Sum of the two numbers is : $sum";
    }

    //Function with reference 
    addFunction(10, 20);
    function addFive($num) {
        $num += 5;
    }
    function addSix(&$num) { //we have to use & when function with reference 
       $num += 6;
    }

    $x = 5;
    addFive($x);
    echo "After add Five: $x";
    addSix($x);
    echo "After add Six: $x";

    
?>