<?php
    declare(strict_types=1); // strict requirement


    //Function with some default arguments
    function setHeight(int $minheight = 50) {
      echo "The height is : $minheight <br>";
    }
    
    setHeight(350);
    setHeight(); // here the value will 50
    setHeight(135);
    setHeight(80);
    
    // Function with returned values.
    function addFunction($num1, $num2) {
       $sum = $num1 + $num2;
       return $sum;
    }
    $return_value = addFunction(10, 20);
    
    echo "Returned value from the function : $return_value";
?>