<?php
$x = 5; // global scope
$a = 3;
$b = 4;
function myTest() {
    $y=6; // local scope
    global $a,$b;
    $b= $a + $b;
  // using x inside this function will generate an error
  echo "<p>Variable x inside function is: " .$x ."</p>";
  echo "<p>Variable y inside function is: " .$y ."</p>";
}
myTest();

echo "<p>Variable x outside function is: ".$x."</p>";
echo "<p>Variable y outside function is: ".$y."</p>";
echo "Sum : " .$b;
?>