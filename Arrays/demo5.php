<?php
    $age = array("sanket"=>"20", "bansal"=>"28", "savan"=>"18");
    $name = array("sanket"=>"20", "bansal"=>"28", "savan"=>"18");
    arsort($age);
    krsort($name);

    //descending order, according to the value
    foreach($age as $x => $x_value) {
    echo "Key=" . $x . ", Value=" . $x_value;
    echo "<br>";
    }

    //descending order, according to the key
    foreach($name as $x => $x_value) {
        echo "Key=" . $x . ", Value=" . $x_value;
        echo "<br>";
    }
?>