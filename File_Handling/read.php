<?php
    $myfile = fopen("filehandle.txt", "r") or die("Unable to open file!");
    echo ("<pre>");

    // echo fread($myfile,filesize("filehandle.txt"));   //     Read whole File

    // echo fgets($myfile);                              // Read Single line of the file

    // while(!feof($myfile)) {                              // Read single line in loop and print.
    //     echo fgets($myfile) . "<br>";
    //   }

    while(!feof($myfile)) {                              //Read single Character in loop and print
        echo fgetc($myfile);
    }

    fclose($myfile);
?>