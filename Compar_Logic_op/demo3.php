<?php
    $x = 100;  
    $y = 50;

    if ($x == 100 and $y == 50) { // and op. same as &&
        echo "Hello world!";
    }
    else{
        echo "wrong data";
    }

    if ($x == 100 or $y == 70) { // or op. same as ||
        echo "Hello world!";
    }
    else{
        echo "wrong data";
    }

    if ($x == 100 xor $y == 50) {
        echo "Hello world!";
    }
    else{
        echo "wrong data"; 
    }

    if ($x !== 90) {
        echo "Hello world!";
    }
?>  