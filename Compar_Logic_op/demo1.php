<?php
    $x = 100;  
    $y = "100";

    var_dump($x == $y); // returns true because values are equal

    var_dump($x === $y); // returns false because types are not equal

    var_dump($x <> $y); // (!=) is same as (<>)  returns false because values are equal 

    $x1 = 10;
    $y1 = 50;

    var_dump($x1 < $y1); // returns true because $x is less than $y
?>  
