<?php
    require('database.php');
    include('auth_session.php');    
    $email = $_SESSION['emailaddress'];
    $sql1 = "SELECT * FROM user_details WHERE e_mail = '$email'";
    $res = mysqli_query($con, $sql1);
    $row = mysqli_fetch_array($res);
    $id = $row['id'];
    
?>

<html>
<head>    
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="./style1.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<title>Registration Form.</title> 
</head> 
 
<body>

<div class="container">
			<div class="row justify-content-center text-center">
				<div class="col-lg-12">
					<h1>Edit User Information</h1>
					<hr> 
				</div>
			</div>
			<div class="row justify-content-center text-center">
				<div class="col-lg-8 mt-3 mb-3">
					<div class="card">  <!-- style="border: 0cm;-->
						<div class="card-body">
							<form action="edit.php" id="form1" method="POST" > 
							<?php require_once 'messages.php'; ?>
								<label for="fname"><b>FirstName: </b></label>
								<input type="text" id="fname" value="<?php echo $row['first_name'];?>" name="firstname"/>
								<br> 
								<br> 
								<label for="lname"><b>LastName: </b></label>
								<input type="text" id="lname" value="<?php echo $row['last_name'];?>" name="lastname"/>
								<br>
								<br>
								<label for="dob"><b>Date of Birth: </b></label>
								<input type="text" id="dob" value="<?php echo $row['dob'];?>" disabled name="dob"/> 
								<br> 
								<br>
								<label for="e_add"><b>Email Address: </b></label>
								<input type="email" id="e_add" value="<?php echo $row['e_mail'];?>" disabled name="emailaddress"/> 
								<br> 
								<br>
								<input class="mt-3 mb-3 btn btn-dark" type="submit" id="submitbtn" value="Update" name="submit"/>
							</form>
						</div>
					</div>
				</div>
			</div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script> 
</body>
</html>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
    if(empty($_POST["firstname"]) ||
        empty($_POST["lastname"]) ){
            $_SESSION["messages"][] = "Please Fill All the details carefullyy.";
            header("location: edit.php");
            exit;
    }
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
	$e_mail = $_POST['emailaddress'];
	
    $sql = "UPDATE user_details SET first_name = '$firstname', last_name = '$lastname' WHERE id = $id";
    
    if (mysqli_query($con, $sql)) {
        $_SESSION["messages"][] = "Data Updated Successfully.";
				header("location: login.php");
				exit;
      } else {
        $_SESSION["messages"][] = "Something gona wrong.";
        header("location: login.php");
        exit;
    }
}
?>
