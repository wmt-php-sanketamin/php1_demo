<?php    
    require('database.php');
    include('auth_session.php');
    $email = $_SESSION['emailaddress'];
        $sql1 = "SELECT * FROM user_details WHERE e_mail = '$email'";
        $res = mysqli_query($con, $sql1);

?>
<html>
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="./style1.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <!-- Bootstrap CSS -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <title>Dashboard</title> 
</head>
<body>
    <div class="container">
		<div class="row justify-content-center text-center">
			<div class="col-lg-12">
                <h2> Hey,
                    Welcome to dashboard page.
                </h2>
			</div>
        </div>
        <div class="row justify-content-center text-center">
			<div class="col-lg-8 mt-3 mb-3">
                <div class="card">
					<div class="card-body d-flex flex-column justify-content-center">
                        <div>
                            <?php
                                $row = mysqli_fetch_assoc($res);
                                echo "<b>User id:</b> " . $row["id"]. " <br> <b>Name:- </b> " . $row["first_name"]. " " . $row["last_name"]. " <br> <b>Date of Birth:- </b> " . $row["dob"] . " <br> <b>Username:- </b> " . $row["e_mail"] . "<br>";
                                
                            ?>
                        </div>
                        <div class="d-flex d-flex justify-content-around">
                                <div class="mt-3 mb-3">
                                <a class="btn btn-dark" href="edit.php" role="button">edit</a>
                                </div>
                                <div class="mt-3 mb-3">
                                <a class="btn btn-dark" href="logout.php" role="button">Logout</a>
                                </div>
                                <div class="mt-3 mb-3">
                                <a class="btn btn-dark" onClick="ondelete()" id="ondelete" role="button">Delete</a>
                                </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
    <script src="./validation.js"></script>
</body>
</html>
