
$().ready(function() {
    $(function() { 
        $( "#my_date_picker" ).datepicker({ 
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: "1930:2021" 
        });
    }); 
    
    $("#form").validate({
        rules : {
            firstname : {
                required : true
            },
            lastname : {
                required : true
            },
            dob :{
                required: true,
                dpDate: true
            },
            emailaddress : {
                required : true,
                email : true
            },
            password : {
                required : true,
                minlength : 8,
            }
        },

        messages: {
            firstname: "Firstname is Required.",

            lastname: "Lastname is Required.",
            dob: {
                required: " * required: You must enter a destruction date",
                date: "Can contain digits only"
            }, 
            emailaddress: {
                required : "Email is required.",
                email : "Ente valid Email."
            }, 
            password: {
                required: "Enter your password",
                minlength: "Minimum password length is 5"
            },
        },

        submitHandler: function(form) {
            form.submit();
        }
    }); 
});
   
function ondelete() {
    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this imaginary file!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
          console.log(willDelete);
        if (willDelete) {
            window.location.href = 'delete.php';
        }
      });
  }
